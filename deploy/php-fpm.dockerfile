FROM php:7.1-fpm-alpine

RUN apk update \
    && apk upgrade \
    && apk add --no-cache bash git libmcrypt libmcrypt-dev curl curl-dev tar coreutils  mysql-client

RUN docker-php-ext-install opcache pdo pdo_mysql mcrypt mbstring tokenizer curl \
    && docker-php-ext-enable curl mcrypt pdo_mysql mbstring opcache

# install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/bin --filename=composer

COPY ./deploy/configs/php.ini /usr/local/etc/php/php.ini
COPY ./deploy/configs/fpm-pool.conf /usr/local/etc/php-fpm.d/yy-www.conf
COPY ./deploy/php-fpm-bootstrap.sh /usr/local/bin/php-fpm-bootstrap.sh

RUN chmod +x /usr/local/bin/php-fpm-bootstrap.sh

CMD ["bash", "-c", "/usr/local/bin/php-fpm-bootstrap.sh"]