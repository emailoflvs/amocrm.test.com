FROM nginx:1.13.1-alpine

RUN set -xe \
    && apk update \
    && apk upgrade \
    && apk add --no-cache bash netcat-openbsd openssl \
    && apk add --no-cache ca-certificates curl

# set the workdir
WORKDIR /app

# nginx php-fpm is configured to check for the physical file existence and php 
# request won't be proxied to php-fpm unless nginx can locate the pysical file
# IMPORTANT: physical file must be identical including the identical absolute
# path. i.e. /app/public/index.php MUST match with exact same path in api service

RUN touch /app/index.php

# copy files - nice trick to copy 2nd file only if it exists
COPY ./deploy/configs/nginx-http.conf /etc/nginx/nginx.conf


