<?php

////////////////////////////////////////////////////
/////////////Дополнительные функции/////////////////
////////////////////////////////////////////////////

/*  функция для очистки данных от лишних символов, HTML и PHP тегов */
function clean($value = "") {
	$value = trim($value);
	$value = stripslashes($value);
	$value = strip_tags($value);
	$value = htmlspecialchars($value);

	return $value;
}


/* показывает содержимое массива */
function ShowArray($array, $way = "")
{
	foreach ($array as $key => $value)
		if (is_array($value)) {
			$way .= " ['$key'] ";
			$str .= ShowArray($value, $way);
		} else
			$str .= "<br>" . $way . " ['" . $key . "'] = '" . $value . "'";

	return $str;
}


/* ищет совпадение данных в массиве */
function SearchInArray(&$array, $search_query = "")
{

	$result = false;
	foreach ($array as $key => $value) {
		if (is_array($value)) {
			$result = SearchInArray($value, $search_query);
			if ($result) {
				return "$key.$result";
			}
		} else {
			$value = (string)$value;
			if (($value == $search_query) !== false)
				return $key;

		}
	}

	return $result;
}


?>
