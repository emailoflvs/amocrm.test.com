<?php

include "header.php";

?>
<div class="container content">
    <form class="form-horizontal" method="post" id="form" action="api.php">
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Имя </label>
            <div class="col-sm-6">
                <input type="text" class="form-control" size="3" id="name" name="name" placeholder="Имя">
            </div>
        </div>
        <div class="form-group">
            <label for="phone" class="col-sm-2 control-label">Телефон <span class="red">*</span></label>
            <div class="col-sm-6">
                <input type="phone" class="form-control" id="phone" name="phone" placeholder="Телефон" required>
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email <span class="red">*</span></label>
            <div class="col-sm-6">
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-6">
                <button type="submit" id="submit" class="btn btn-primary">Отправить</button>
                <div></div>
            </div>
        </div>
    </form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="jv/dist/jquery.validate.js"></script>

<script>
    $(function(){
        $('#form').validate({
            rules: {
                phone: {
                    required: true,
                    minlength: 2
                }
            },
            messages: {
                phone: {
                    required: "Поле 'Телефон' обязательно к заполнению",
                    minlength: "Введите не менее 5-х символов в поле 'Телефон'"
                },
                email: {
                    required: "Поле 'Email' обязательно к заполнению",
                    email: "Необходим формат адреса email"
                }            }
        });
    });
</script>

<?
include("footer.php");
?>
