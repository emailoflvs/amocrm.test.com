<?php

namespace Models;

class Account
{

	public $user_login;        #логин (электронная почта)
	public $user_hash;        #Хэш для доступа к API
	public $server;        #сервер для подключения API

	public $admin_email;    #почта админа
	public $link_auth = '/private/api/auth.php?type=json';    #ссылка для авторизации
	public $link_account = '/api/v2/account';            #ссылка для аккаунта
	public $link_contacts = '/api/v2/contacts';            #ссылка для контактов
	public $link_leads = '/api/v2/leads';                #ссылка для сделок
	public $link_tasks = '/api/v2/tasks';                #ссылка для заданий

	function __construct($user_login = "", $user_hash = "", $server = "")
	{
		$this->user_login = $user_login;
		$this->user_hash = $user_hash;
		$this->server = $server;
		$this->link_auth = $server . $this->link_auth;
		$this->link_account = $server . $this->link_account;
		$this->link_contacts = $server . $this->link_contacts;
		$this->link_leads = $server . $this->link_leads;
		$this->link_tasks = $server . $this->link_tasks;
	}

	/* Запрос к серверу */
	function ServerRequest($link, $request = "")
	{
		$curl = curl_init(); #Сохраняем дескриптор сеанса cURL
		#Устанавливаем необходимые опции для сеанса cURL
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');

		curl_setopt($curl, CURLOPT_URL, $link);

		#отправляем данные на сервер методом POST
		if ($request) {
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($request));
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		}

		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
		curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		$out = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
		$code = curl_getinfo($curl, CURLINFO_HTTP_CODE); #Получим HTTP-код ответа сервера
		curl_close($curl); #Завершаем сеанс cURL

		# Теперь мы можем обработать ответ, полученный от сервера. Это пример. Вы можете обработать данные своим способом.
		$code = (int)$code;
		$errors = array(
			301 => 'Moved permanently',
			400 => 'Bad request',
			401 => 'Unauthorized',
			403 => 'Forbidden',
			404 => 'Not found',
			500 => 'Internal server error',
			502 => 'Bad gateway',
			503 => 'Service unavailable'
		);
		try {
			#Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
			//if ($code != 200 && $code != 204)
			//	throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
		} catch (Exception $E) {
			if ($link != $this->link_auth) {
				$this->Authorisation();

			}
			die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
			//echo '<br>Ошибка: '.$E->getMessage().PHP_EOL.'Код ошибки: '.$E->getCode();
		}

		/*
		 Данные получаем в формате JSON, поэтому, для получения читаемых данных,
		 нам придётся перевести ответ в формат, понятный PHP
		 */
		$Response = json_decode($out, true);

		return $Response;
	}

	/* авторизация */
	function Authorisation()
	{
		#данные для авторизации
		$user = [
			'USER_LOGIN' => $this->user_login,
			'USER_HASH' => $this->user_hash
		];

		$response = $this->ServerRequest($this->link_auth, $user);

		$response = $response['response'];

		return ($response['auth']); #возвращаю результат аутентификации
	}

	///////////////////////////////////////////////
	///////АККАУНТ/////////////////////////////////
	///////////////////////////////////////////////

	/* возвращает id текущего пользователя */
	function GetCurrentUser()
	{
		$response = $this->ServerRequest($this->link_account);
		return $response['current_user'];
	}

	/* проверяет является ли пользователь админом */
	function IsAdmin($user_id = '0')
	{
		$response = $this->ServerRequest($this->link_account . "?with=users&free_users=Y");

		if (!$user_id)
			$user_id = $this->GetCurrentUser();

		foreach ($response['_embedded']['users'] as $key => $value) {
			if ($key == $user_id)
				return $value['is_admin'];
		}
		return 0;
	}

	///////////////////////////////////////////////
	///////РАБОТА С КОНТАКТАМИ/////////////////////
	///////////////////////////////////////////////

	/* поиск контакта по запросу - возвращает id контакта */
	function SearchContact($search_query)
	{
		$response = $this->ServerRequest($this->link_contacts);

		$result = 0;
		#перебирает список контактов
		if ($response ['_embedded']['items'])
			foreach ($response ['_embedded']['items'] as $key => $value) {

				$result = SearchInArray($response ['_embedded']['items'][$key], $search_query);

				if ($result)
					return $response ['_embedded']['items'][$key]['id'];
			}

		return false;
	}


	/* возвращает пользователя, ответственного за контакт */
	function GetContactRespUser($contact_id)
	{

		$response = $this->ServerRequest($this->link_contacts);
		$response = $response ['_embedded']['items'];

		if ($response)
			foreach ($response as $key => $value) {
				if ($response[$key]['id'] == $contact_id)
					return $response[$key]['responsible_user_id'];

			}
		return 0;
	}

	/* ДОБАВЛЕНИЕ КОНТАКТА */
	function AddContact($contact_data)
	{
		$response = $this->ServerRequest($this->link_contacts, $contact_data);
		$response = $response['_embedded']['items']['0']['id'];

		if ($response) {
			return $response;
		} else {
			echo "Ошибка. Контакт не добавлен.";
			return false;
		}
	}


	///////////////////////////////////////////////
	///////РАБОТА СО СДЕЛКАМИ/////////////////////
	///////////////////////////////////////////////


	/* возвращает пользователя, ответственного за сделку */
	function GetLeadRespUser($lead_id)
	{
		$response = $this->ServerRequest($this->link_leads);
		$response = $response ['_embedded']['items'];

		if ($response)
			foreach ($response as $key => $value) {
				if ($response[$key]['id'] == $lead_id)
					return $response[$key]['responsible_user_id'];
			}
		return 0;
	}


	/* поиск пользователя для назначения его ответственным (метод распределения) */
	function FreeResponsibleUser()
	{
		$contacts = "";      #строка, в которую мы будем добавлять ID контактов
		$users = [];         #массив с пользователями

		#получаем список сделок
		$response = $this->ServerRequest($this->link_leads);

		$response = $response ['_embedded']['items'];
		if ($response)
			foreach ($response as $key => $value) {
				if ($response[$key]['created_at'] >= (time() - 60 * 60 * 24)) {   #отбираем сделки за последние сутки

					if (isset($response[$key]['main_contact']['id'])) {            #контакт этой сделки
						$contact_id = $response[$key]['main_contact']['id'];
						$responsible_user_id = $this->GetContactRespUser($contact_id); #пользователь, ответственный за этот контакт

						if (!$this->IsAdmin($responsible_user_id))
							#если пользователь не админ, то проверяем, добавляли ли мы уже этот контакт в список проверенных
							if (!strstr($contacts, $contact_id)) {

								#если контакт в список проверенных контактов не добавлялся, то добавляем пользователю еще одну сделку
								$users[$responsible_user_id]++;
								$contacts .= " $contact_id "; #добавляем id контакта в список проверенных
							}
					}
				}
			}

		#сортируем массив пользователей по количеству сделок и берем первый ключ - это свободный пользователь
		asort($users);
		$responsible_user_id = key($users);

		return $responsible_user_id;
	}

	/* ДОБАВЛЕНИЕ СДЕЛКИ*/
	function AddLead($lead_name = "", $status_id = 1, $sale = 0, $contacts_id = 0)
	{

		#получаем id менеджера ответственного за контакт
		$responsible_user_id = $this->GetContactRespUser($contacts_id);

		$lead['add'] = [
			[
				'name' => $lead_name,          #название
				'created_at' => time(),        #дата добавления
				'status_id' => $status_id,     #статус
				'sale' => $sale,               #сумма сделки
				'responsible_user_id' => $responsible_user_id,  #ответственный за сделку
				'contacts_id' =>
					[
						$contacts_id             # ID контакта для сделки
					],
			]
		];

		#запрос для добавление сделки
		$response = $this->ServerRequest($this->link_leads, $lead);

		$response = $response['_embedded']['items'][0]['id']; # id первой добавленной сделки

		if ($response) {
			echo "<br>Сделка добавлена. ID: $response<br>";
			return $response;
		} else {
			echo "<br>Ошибка. Сделка не добавлена.<br>";
			return false;
		}
	}


	/* ДОБАВЛЕНИЕ ЗАДАЧИ*/

	function AddTask($lead_id, $element_type, $task_type = 1, $comments = "")
	{

		$responsible_user_id = $this->GetLeadRespUser($lead_id);

		$tasks['add'] = [
			[
				'element_id' => $lead_id,        #ID сделки
				'element_type' => $element_type, #Показываем, к чему привязывваем: сделка или контакт
				'task_type' => $task_type,       #Тип задачи
				'text' => $comments,
				'responsible_user_id' => $responsible_user_id,
				'complete_till_at' => (time() + 60 * 60 * 24)
			]
		];

		#запрос для добавления задания
		$response = $this->ServerRequest($this->link_tasks, $tasks);

		$response = $response['_embedded']['items'][0]['id']; # id первого добавленного задания

		if ($response) {
			echo "<br>Задача добавлена.ID: $response<br>";
			return $response;
		} else {
			echo "<br>Ошибка. Задача не добавлена.<br>";
			return false;
		}
	}

	/*  ОТПРАВКА ПОЧТЫ */
	function SendEmail($name = "", $email = "", $phone = "", $message = "")
	{
		$message = "Заказ с сайта
		От: $name
		Тел.: $phone
		Email: $email
		$message
		";
		$subject = "Заказ с сайта от $name";

		mail($this->admin_email, $subject, $message, "From: $name <$email>\r\n" . "Reply-To: $name <$email>\r\n" . "X-Mailer: PHP/" . phpversion());
	}
}
