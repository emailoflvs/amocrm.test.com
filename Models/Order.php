<?php

namespace Models;

class Order
{
	/*
	* Объект с данными, полученными после отправки формы
	*/
	public $name, $phone, $email;
	public $error;
	public $contact_id, $contact_data, $lead_id;
	public $status_id, $sale, $element_type, $task_type, $comments;

	function __construct($name = "", $phone = "", $email = "")
	{
		$this->name = $name;
		$this->phone = $phone;
		$this->email = $email;
	}

	function Validation()
	{

		if (!empty($this->name))
			$this->name = clean($this->name);

		if (!empty($this->phone)) {
			$this->phone = clean($this->phone);

			if (!preg_match("/^[0-9]/", str_replace(['+', '-'], ['', ''], $this->phone))) {
				$this->error = "Поле 'Телефон' содержит некорректные символы: '" . $this->phone . "'";
				return false;
			}

		} else {
			$this->error = "Поле 'Телефон' не заполнено";
			return false;
		}


		if (!empty ($this->email)) {
			$this->email = clean($this->email);

			if (!preg_match("/^[0-9a-z_^\-\.]+@[0-9a-z_^\-^\.]+\.[a-z]{2,6}$/i", $this->email)) {
				$this->error = "Поле 'Email' содержит некорректные символы: '" . $this->email . "''";
				return false;
			}
			if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
				$this->error = "Поле 'Email' заполнено не правильно";
				return false;
			}

		} else {
			$this->error = "Поле 'Email' не заполнено";
			return false;
		}

		return true;
	}

	function ShowInfo()
	{
		return "Данные: " . $this->name . ", " . $this->phone . ", " . $this->email . ", " . $this->message;
	}

	function AdditionalVars($status_id = 0, $sale = 0, $element_type = 0, $task_type = 0, $comments = "")
	{
		$this->status_id = $status_id;  # статус сделки
		$this->sale = $sale;     #объем продаж
		$this->element_type = $element_type; #Показываем, привязываем к сделке или к контакту
		$this->task_type = $task_type;   #Тип задания
		$this->comments = $comments;
	}

}