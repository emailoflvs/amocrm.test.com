<?php
namespace Api;

require_once __DIR__ . '/vendor/autoload.php';

include("header.php");
include("functions.php");


use Models\Account;
use Models\Order;


//$order = new Order('', '', '');

//var_dump($order);
//die();

$order = new Order($_POST['name'], $_POST['phone'], $_POST['email']);

if (!$order->Validation())
	echo "<br>" . $order->error . "<br>";
else
	echo "<br>" . $order->ShowInfo();

echo '<br><br><a onclick="javascript:history.back(); return false;" href="" alt="Вернуться на предыдущую страницу"><< вернуться</a><br>';

$account = new Account;

include("private.php");

/*if ($account->Authorisation ())
	echo 'Авторизация прошла успешно<br><br>';
else
	echo 'Авторизация не удалась<br><br>';
*/

$order->contact_id = $account->SearchContact($order->email);       #поиск контакта в базе по email

#поиск контакта в базе по телефону
$order->contact_id = (!$order->contact_id) ? $account->SearchContact($order->phone) : 0;


if ($order->contact_id) {

	#если контакт существует просто показываю данные
	echo "<p>Контакт существует. 
            <br>Contact_id=$order->contact_id. 
			<br>Ответственный за контакт:" . $account->GetContactRespUser($order->contact_id) .
		"<br><br></p>";

} else {

	#Если контакта нет, то добавляем
	$order->contact_data ['add'] = [
		[
			'name' => $order->name,
			'responsible_user_id' => $account->FreeResponsibleUser(), #находим наиболее свовбодного менеджера по принципу распределения
			'created_by' => $account->GetCurrentUser(),               #id текущего пользователя
			'created_at' => time(),                              #время добавления

			'custom_fields' => [
				[
					'values' => [
						[
							'value' => $order->phone,
							'enum' => "WORK"
						],
					],
				],
				[
					'values' => [
						[
							'value' => $order->email,
							'enum' => "email"
						],
					]
				]
			]
		]
	];

	$order->contact_id = $account->AddContact($order->contact_data);   #ID добавленного контакта

	if ($order->contact_id)
		echo "<br>Контакт добавлен.<br>";

}

/*
    отправляем почту админу
*/
$account->SendEmail($order->name, $order->email, $order->phone);

/*
	Добавление сделки к контакту
*/

$order->AdditionalVars(13667501, 1000, 2, 1, "first contact");

$order->lead_id = $account->AddLead('Order from site', $order->status_id, $order->sale, $order->contact_id);

/*
	Добавление задания к сделке
*/

$account->AddTask($order->lead_id, $order->element_type, $order->task_type, $order->comments);


?>

<br><br><a onclick="javascript:history.back(); return false;" href="" alt="Вернуться на предыдущую страницу"><<
    вернуться</a>


<?
include("footer.php");
?>



